import {DatePipe} from '@angular/common';
import {OnDestroy, Pipe, PipeTransform} from '@angular/core';
import {Subscription} from 'rxjs';
import {Lang} from '../../../models/Lang';
import {LangService} from '../../../services/lang.service';

@Pipe({
  name: 'translatedDate',
})
export class TranslatedDatePipe implements PipeTransform, OnDestroy {
  private lang: Lang;
  private subscription: Subscription;

  constructor(
    private langService: LangService
  ) {
    langService.lang$().subscribe((lang: Lang) => this.lang = lang);
  }

  public ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  transform(value: Date): unknown {
    const datePipe: DatePipe = new DatePipe(this.lang);
    return datePipe.transform(value, 'MMMM yyyy');
  }
}
