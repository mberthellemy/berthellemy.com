import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {
  clearSelectedKeywordsAction,
  defineSelectedKeywordsAction,
  showHeatmapAction,
  showSelectedAction
} from '../../../ngrx/actions/keyword.action';
import {selectedKeywordsSelector, showHeatmapSelector, showSelectedSelector} from '../../../ngrx/selectors/keywords.selector';

@Injectable()
export class KeywordService {

  constructor(
    private store: Store
  ) {}

  public setHeatmapStatus(value: boolean): void {
      this.store.dispatch(showHeatmapAction({show: value}));
  }

  public setSelectedStatus(value: boolean): void {
    this.store.dispatch(showSelectedAction({show: value}));
  }

  public defineSelectedKeywords(keywords: string[]): void {
    this.store.dispatch(defineSelectedKeywordsAction({selectedKeywords: keywords}));
  }

  public selectedKeywords$(): Observable<string[]> {
    return this.store.select(selectedKeywordsSelector);
  }

  public clearKeywordsSelection(): void {
    this.store.dispatch(clearSelectedKeywordsAction());
  }

  public showHeatmap$(): Observable<boolean> {
    return this.store.select(showHeatmapSelector);
  }

  public showSelected$(): Observable<boolean> {
    return this.store.select(showSelectedSelector);
  }
}
