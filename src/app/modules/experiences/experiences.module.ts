import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatIconModule} from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatStepperModule} from '@angular/material/stepper';
import {RouterModule, Routes} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../HttpLoaderFactory';
import {ToolsModule} from '../tools/tools.module';
import {EventComponent} from './components/event/event.component';
import {ExperiencesComponent} from './components/experiences.component';
import {KeywordsComponent} from './components/keywords/keywords.component';
import {ProjectComponent} from './components/project/project.component';
import {ProjectsPageComponent} from './components/projects-page/projects-page.component';
import {ProjectsComponent} from './components/projects/projects.component';
import {ReviewComponent} from './components/review/review.component';
import {ReviewsPageComponent} from './components/reviews-page/reviews-page.component';
import {ReviewsComponent} from './components/reviews/reviews.component';
import {TranslatedDatePipe} from './pipes/date.pipe';
import {KeywordService} from './services/keyword.service';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/not-found' },
  { path: 'projects', component: ProjectsPageComponent },
  { path: 'reviews', component: ReviewsPageComponent },
  { path: '**', pathMatch: 'full', redirectTo: '/not-found' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToolsModule,
    MatChipsModule,
    MatButtonModule,
    MatCardModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    FormsModule,
    MatIconModule,
    MatStepperModule,
  ],
  declarations: [
    ProjectComponent,
    EventComponent,
    ExperiencesComponent,
    ProjectsPageComponent,
    ReviewsPageComponent,
    ReviewsComponent,
    ReviewComponent,
    ProjectsComponent,
    KeywordsComponent,
    TranslatedDatePipe
  ],
  providers: [
    KeywordService,
    {
      provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
    }
  ]
})
export class ExperiencesModule { }
