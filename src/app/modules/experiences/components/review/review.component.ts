import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Review} from '../../../../models/Review';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewComponent {
  @Input() review: Review;
}
