import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Lang} from '../../../../models/Lang';
import {reviews} from './reviews';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewsComponent {
  public reviews = reviews;
}
