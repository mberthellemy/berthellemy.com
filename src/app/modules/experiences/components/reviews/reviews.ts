import {Review} from '../../../../models/Review';

export const reviews: Review[] = [{
  i18nKey: 'PC',
  name: 'Paul Carraud',
  company: 'Capgemini',
  year: 2020,
  linkedinLink: 'https://www.linkedin.com/in/paul-carraud-%E2%99%A0-capgemini-%E2%99%A0-62857813/'
}, {
  i18nKey: 'ZM',
  name: 'Zakkarya Mekhalfa',
  company: 'Orano',
  year: 2020,
  linkedinLink: 'https://www.linkedin.com/in/zakkarya-mekhalfa-911882183/'
} , {
  i18nKey: 'CL',
  name: 'Camille Leplumey',
  company: 'Capgemini',
  year: 2020,
  linkedinLink: 'https://www.linkedin.com/in/camille-leplumey-b83a0b166/'
}, {
  i18nKey: 'GF',
  name: 'Ghislain Ferraroli',
  company: 'Capgemini',
  year: 2020,
  linkedinLink: 'https://www.linkedin.com/in/ghislain-ferraroli/'
}, {
  i18nKey: 'SB',
  name: 'Sonia B.',
  company: 'Capgemini',
  year: 2019,
}, {
  i18nKey: 'MB',
  name: 'Meriem Belhocine',
  company: 'Capgemini',
  year: 2019,
  linkedinLink: 'https://www.linkedin.com/in/meriem-belhocine/'
}];
