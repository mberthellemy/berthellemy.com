import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Project} from '../../../../models/Project';
import {KeywordService} from '../../services/keyword.service';
import {projects} from './projects';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectsComponent {
  public projects = projects;
  public overlighedProject = {
    i18nKey: 'landing',
    type: 'project',
    date: {
      start: new Date('04/20/2021')
    },
    imagePath: './assets/images/landing.png',
    source: 'https://gitlab.com/mberthellemy/berthellemy.com',
    icon: 'flag',
    tags: ['Responsive', 'Javascript', 'Typescript', 'Angular', 'Material', 'Redux', 'NgRx', 'Frontend', 'Design', 'Gitlab', 'Git'],
  };

  constructor(
      private keywordsService: KeywordService
  ) {}

  get years(): number[] {
    const years = new Set<number>();
    for (const project of this.projects) {
      const year = project.date.start.getFullYear();
      years.add(year);
    }
    const sorted = [...years].sort();
    const first = sorted[0];
    return [...years, first - 1].sort().reverse();
  }


  get groupedProjectByYear(): {[year: number]: Project[]} {
    const output = {};
    for (const project of this.projects) {
      if (output[project.date.start.getFullYear()] === undefined) {
        output[project.date.start.getFullYear()] = [];
      }
      output[project.date.start.getFullYear()].push(project);
    }
    return output;
  }


  public setHoveredProject(experience: Project): void {
    this.keywordsService.defineSelectedKeywords(experience.tags);
  }

  public unsetHoveredProject(): void {
    this.keywordsService.clearKeywordsSelection();
  }


  private goToLink(url: string): void {
    window.open(url, '_blank');
  }
}
