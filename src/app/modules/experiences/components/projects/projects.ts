import {Project} from '../../../../models/Project';

export const projects: Project[] = [
  {
    i18nKey: 'edf',
    type: 'project',
    date: {
      start: new Date('07/01/2021'),
      end: new Date('10/20/2020')
    },
    imagePath: './assets/images/edf.png',
    icon: 'bolt',
    tags: ['Material', 'Gitlab', 'ApexChart', 'Git', 'Redux', 'NgRx', 'API', 'Typescript', 'Javascript', 'Database', 'MongoDB', 'Angular', 'Frontend', 'Backend', 'Fullstack', 'Design', 'Typescript', 'Optimization', 'ElectronJS', 'Performance', 'ChartJS', 'NodeJS', 'ExpressJS', 'GraphQL', 'TechLead', 'Energy'],
  }, {
  i18nKey: 'computeTS',
  type: 'project',
  date: {
    start: new Date('06/01/2021'),
    end: new Date('05/01/2021')
  },
  imagePath: './assets/images/compute.png',
  icon: 'engineering',
  tags: ['Typescript', 'Javascript', 'Library', 'Performance', 'Conception', 'Documentation', 'Gitlab', 'Git'],
  link: 'https://www.npmjs.com/package/compute.ts'
}, {
  i18nKey: 'freemate',
  type: 'project',
  date: {
    start: new Date('02/01/2021'),
    end: new Date('04/15/2021')
  },
  imagePath: './assets/images/freemate.png',
  icon: 'groups',
  tags: ['Docker', 'Responsive', 'API', 'Typescript', 'Javascript', 'Angular', 'Frontend', 'Design', 'Backend', 'NodeJS', 'ExpressJS', 'Material', 'MongoDB', 'Database', 'Architect', 'Conception', 'Fullstack', 'Gitlab', 'Git'],
  link: 'https://app.freemate.tech'
}, {
  i18nKey: 'dassault',
  type: 'project',
  date: {
    start: new Date('09/01/2020'),
    end: new Date('12/31/2020')
  },
  imagePath: './assets/images/dassault.png',
  icon: 'flight',
  tags: ['Database', 'Worker', 'API', 'Javascript', 'Angular', 'ReactJS', 'Redux', 'Frontend', 'Design', 'Backend',
    'Fullstack', 'ExpressJS', 'NodeJS', 'RabbitMQ', 'Broker', 'STOMP', 'MQTT', 'ElectronJS', 'Performance', 'Gitlab', 'Git', 'Defense', 'Aeronautic'],
},  {
  i18nKey: 'freelance',
  type: 'event',
  date: {
    start: new Date('09/01/2020')
  },
  icon: 'engineering',
  tags: []
}, {
  i18nKey: 'orano',
  type: 'project',
  date: {
    start: new Date('02/01/2018'),
    end: new Date('07/01/2020')
  },
  imagePath: './assets/images/orano.png',
  icon: 'auto_delete',
  tags: ['API', 'Javascript', 'MapboxGL', 'Database', 'Angular',
    'Frontend', 'Design', 'Backend', 'Fullstack', 'Typescript', 'ElectronJS', 'NodeJS', 'ExpressJS', 'MongoDB', 'Architect', 'Conception', 'TechLead', 'Docker', 'Gitlab', 'Git', 'Nuclear', 'Energy'],
}, {
  i18nKey: 'cnes',
  type: 'project',
  date: {
    start: new Date('03/01/2020'),
    end: new Date('07/01/2020')
  },
  imagePath: './assets/images/cnes.png',
  icon: 'description',
  tags: ['Gitlab', 'Git', 'API', 'Typescript', 'Javascript',
    'Database', 'TechLead', 'Material', 'Frontend', 'Design', 'Science', 'CouchDB'],
}, {
  i18nKey: 'jc-decaux',
  type: 'project',
  date: {
    start: new Date('02/01/2017'),
    end: new Date('09/01/2017')
  },
  imagePath: './assets/images/jcdecaux.png',
  icon: 'cast',
  tags: ['Solver', 'AI', 'API', 'Javascript', 'Database', 'SQLServer', 'AngularJS', 'MapboxGL', 'Design', 'JEE', 'Frontend', 'Backend', 'Fullstack', 'LocalSolver', 'SVN'],
}, {
  i18nKey: 'upmc',
  type: 'event',
  date: {
    start: new Date('06/01/2016')
  },
  icon: 'school',
  tags: []
}, {
  i18nKey: 'fm-logistic',
  type: 'project',
  date: {
    start: new Date('02/01/2016'),
    end: new Date('09/01/2016')
  },
  imagePath: './assets/images/fm-logistic.png',
  icon: 'inventory_2',
  tags: ['LocalSolver', 'Solver', 'AI', 'Javascript', 'Python', 'Optimization', 'Performance', 'SVN'],
}, {
  i18nKey: 'localsolver',
  type: 'event',
  date: {
    start: new Date('01/01/2017')
  },
  icon: 'badge',
  tags: []
},
  {
    i18nKey: 'capgemini',
    type: 'event',
    date: {
      start: new Date('01/01/2018')
    },
    icon: 'badge',
    tags: []
  }
];
