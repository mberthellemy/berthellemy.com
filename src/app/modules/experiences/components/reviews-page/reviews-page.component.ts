import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-reviews-page',
  templateUrl: './reviews-page.component.html',
  styleUrls: ['./reviews-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReviewsPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
