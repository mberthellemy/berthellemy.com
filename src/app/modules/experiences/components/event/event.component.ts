import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Project} from '../../../../models/Project';
import {KeywordService} from '../../services/keyword.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventComponent {
  @Input() project: Project;

}
