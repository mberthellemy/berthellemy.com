import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Project} from '../../../../models/Project';
import {KeywordService} from '../../services/keyword.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectComponent {
  @Input() project: Project;

  private goToLink(url: string): void {
    window.open(url, '_blank');
  }
}
