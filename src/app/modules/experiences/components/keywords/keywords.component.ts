import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {projects} from '../projects/projects';
import {Lang} from '../../../../models/Lang';
import {KeywordService} from '../../services/keyword.service';

@Component({
  selector: 'app-keywords',
  templateUrl: './keywords.component.html',
  styleUrls: ['./keywords.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KeywordsComponent {

  @Input() isMobile: boolean;

  public mappedKeywordsBackgroundColors = this.builMappedKeywordsBackgroundColors();
  public mappedKeywordsColors = this.builMappedKeywordsColors();
  public heatmapSelected$: Observable<boolean>;
  public selectedKeywords$: Observable<Set<string>>;
  public showSelectedKeywords$: Observable<boolean>;

  public title = {
    [Lang.FRENCH]: 'Mots clés',
    [Lang.ENGLISH]: 'Keywords'
  };

  get keywords(): string[] {
    return Object.keys(this.mappedKeywordsColors);
  }

  constructor(
    private keywordsService: KeywordService
  ) {
    this.showSelectedKeywords$ = keywordsService.showSelected$();
    this.heatmapSelected$ = keywordsService.showHeatmap$();
    this.selectedKeywords$ = keywordsService.selectedKeywords$().pipe(map((keywords: string[]) => new Set<string>(keywords)));
  }

  public toggleHoverStatus(status: boolean): void {
    this.keywordsService.setSelectedStatus(status);
  }

  public toggleHeatmapStatus(status: boolean): void {
    this.keywordsService.setHeatmapStatus(status);
  }

  public buildKeywordOccurences(): {[keyword: string]: number} {
    const output = {};
    for (const experiece of projects) {
      for (const tag of experiece.tags) {
        if (output[tag] === undefined) {
          output[tag] = 0;
        }
        output[tag]++;
      }
    }
    return output;
  }


  private builMappedKeywordsColors(): {[keyword: string]: string} {
    const output = {};
    const keywordOccurences = this.buildKeywordOccurences();
    const maxOccurence = Math.max(...Object.values(keywordOccurences));

    for (const keyword of Object.keys(keywordOccurences)) {
      const tagOccurence = keywordOccurences[keyword];
      const ratio = tagOccurence / maxOccurence;
      if (ratio > 0.3) {
        output[keyword] = 'white';
      } else if (ratio > 0.2) {
        output[keyword] = 'white';
      } else if (ratio > 0.1) {
        output[keyword] = 'white';
      } else if (ratio > 0.05) {
        output[keyword] = 'white';
      } else {
        output[keyword] = 'black';
      }
    }
    return output;
  }

  private builMappedKeywordsBackgroundColors(): {[keyword: string]: string} {
    const output = {};
    const keywordOccurences = this.buildKeywordOccurences();
    const maxOccurence = Math.max(...Object.values(keywordOccurences));

    for (const keyword of Object.keys(keywordOccurences)) {
      const tagOccurence = keywordOccurences[keyword];
      const ratio = tagOccurence / maxOccurence;
      if (ratio > 0.3) {
        output[keyword] = '#3f51b5';
      } else if (ratio > 0.2) {
        output[keyword] = '#455298';
      } else if (ratio > 0.1) {
        output[keyword] = '#727cb7';
      } else if (ratio > 0.05) {
        output[keyword] = '#929bd5';
      } else {
        output[keyword] = '#bec4e8';
      }
    }
    return output;
  }
}
