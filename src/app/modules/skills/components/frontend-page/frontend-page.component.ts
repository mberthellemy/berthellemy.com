import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-frontend-page',
  templateUrl: './frontend-page.component.html',
  styleUrls: ['./frontend-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FrontendPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
