import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-teamwork-page',
  templateUrl: './teamwork-page.component.html',
  styleUrls: ['./teamwork-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TeamworkPageComponent {

}
