import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-backend-page',
  templateUrl: './backend-page.component.html',
  styleUrls: ['./backend-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BackendPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
