import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {RouterModule, Routes} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../HttpLoaderFactory';
import {ToolsModule} from '../tools/tools.module';
import {BackendPageComponent} from './components/backend-page/backend-page.component';
import {FrontendPageComponent} from './components/frontend-page/frontend-page.component';
import {TeamworkPageComponent} from './components/teamwork-page/teamwork-page.component';
import {SkillsComponent} from './components/skills.component';


const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: '/not-found' },
    { path: 'teamwork', component: TeamworkPageComponent },
    { path: 'frontend', component: FrontendPageComponent },
    { path: 'backend', component: BackendPageComponent },
    { path: '**', pathMatch: 'full', redirectTo: '/not-found'}
];

@NgModule({
    declarations: [
        SkillsComponent,
        TeamworkPageComponent,
        FrontendPageComponent,
        BackendPageComponent
    ],
    imports: [
        CommonModule,
        ToolsModule,
        MatSnackBarModule,
        RouterModule.forChild(routes),
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
    ]
})
export class SkillsModule { }
