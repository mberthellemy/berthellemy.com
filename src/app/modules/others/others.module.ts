import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {RouterModule, Routes} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../HttpLoaderFactory';
import {ToolsModule} from '../tools/tools.module';
import {CgvPageComponent} from './components/cgv-page/cgv-page.component';
import {PricingPageComponent} from './components/princing-page/pricing-page.component';
import {MatCardModule} from "@angular/material/card";
import {MatListModule} from "@angular/material/list";
import {MatCheckboxModule} from "@angular/material/checkbox";


const routes: Routes = [
  { path: 'gcs', component: CgvPageComponent },
  { path: 'pricing', component: PricingPageComponent }
];

@NgModule({
  declarations: [
    CgvPageComponent,
    PricingPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToolsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatCheckboxModule
  ]
})
export class OthersModule {}
