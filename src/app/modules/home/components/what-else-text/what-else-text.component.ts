import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-what-else-text',
  templateUrl: './what-else-text.component.html',
  styleUrls: ['./what-else-text.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WhatElseTextComponent {}
