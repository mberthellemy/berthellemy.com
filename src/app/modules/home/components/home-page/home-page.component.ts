import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {fromEvent} from 'rxjs';
import {LangService} from '../../../../services/lang.service';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePageComponent implements OnInit {
  public scrolled = false;

  constructor(
    public langService: LangService,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public titleService: Title,
  ) {
    fromEvent(window, 'scroll')
      .subscribe(() => this.updateScrollState());
  }

  public scollToTop(): void {
    document.querySelector('#top').scrollIntoView();
    this.scrolled = true;
  }

  private updateScrollState(): void {
    if (window.pageYOffset >= window.innerHeight && this.scrolled === false && this.isMobile === false) {
      this.scrolled = true;
    }
  }

  get isMobile(): boolean {
    return window.innerWidth < 760;
  }

  public ngOnInit(): void {
    this.updateScrollState();
  }
}
