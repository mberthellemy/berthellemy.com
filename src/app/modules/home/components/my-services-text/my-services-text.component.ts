import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-my-services-text',
  templateUrl: './my-services-text.component.html',
  styleUrls: ['./my-services-text.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyServicesTextComponent {}
