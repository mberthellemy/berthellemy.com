import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-who-am-i-text',
  templateUrl: './who-am-i-text.component.html',
  styleUrls: ['./who-am-i-text.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WhoAmITextComponent {}
