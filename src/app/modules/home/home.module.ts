import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../HttpLoaderFactory';
import {ToolsModule} from '../tools/tools.module';
import {HomePageComponent} from './components/home-page/home-page.component';
import {MyServicesTextComponent} from './components/my-services-text/my-services-text.component';
import {WhatElseTextComponent} from './components/what-else-text/what-else-text.component';
import {WhoAmITextComponent} from './components/who-am-i-text/who-am-i-text.component';


const routes: Routes = [
  { path: '', component: HomePageComponent }
];

@NgModule({
  declarations: [
    HomePageComponent,
    WhoAmITextComponent,
    MyServicesTextComponent,
    WhatElseTextComponent
  ],
  imports: [
    CommonModule,
    ToolsModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ]
})
export class HomeModule { }
