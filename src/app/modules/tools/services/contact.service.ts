import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {delay} from 'rxjs/operators';
import {Lang} from '../../../models/Lang';

@Injectable()
export class ContactService {
  constructor(
    private httpClient: HttpClient
  ) {}

  public sendMessage(email: string, message: string): Observable<any> {
    const formData: any = new FormData();
    formData.append('email', email);
    formData.append('message', message);
    return this.httpClient.post('https://formspree.io/mayppjdd', formData).pipe(delay(1000));
  }
}
