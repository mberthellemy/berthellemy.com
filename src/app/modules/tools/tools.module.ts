import {CommonModule} from '@angular/common';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../../HttpLoaderFactory';
import {AvailabilityComponent} from './components/availability/availability.component';
import {BluredInTextComponent} from './components/blured-in-text/blured-in-text.component';
import {CallToActionComponent} from './components/call-to-action/call-to-action.component';
import {GitlabButtonComponent} from './components/gitlab-button/gitlab-button.component';
import {ParagraphComponent} from './components/paragraph/paragraph.component';
import {ContactService} from './services/contact.service';


@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatIconModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    BluredInTextComponent,
    ParagraphComponent,
    AvailabilityComponent,
    CallToActionComponent,
    GitlabButtonComponent
  ],
  exports: [
    BluredInTextComponent,
    ParagraphComponent,
    AvailabilityComponent,
    CallToActionComponent,
    GitlabButtonComponent
  ],
  providers: [
    ContactService
  ]
})
export class ToolsModule { }
