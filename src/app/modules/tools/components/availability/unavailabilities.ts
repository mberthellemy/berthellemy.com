export const unavailibilities = [];

// Flight
unavailibilities.push(new Date('05/31/2021'));

// Sicile
for (let i = 5; i <= 15; i++) {
  unavailibilities.push(new Date('08/' + i + '/2021'));
}

// Sud
for (let i = 1; i <= 31; i++) {
  unavailibilities.push(new Date('07/' + i + '/2021'));
}
