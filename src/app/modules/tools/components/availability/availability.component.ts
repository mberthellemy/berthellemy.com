import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {Availability} from '../../../../models/Availability';
import {availabilitySelector} from '../../../../ngrx/selectors/informations.selector';
import {unavailibilities} from './unavailabilities';

@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvailabilityComponent {
  public Availability = Availability;

  public availability$: Observable<Availability>;
  public selectedDate: Date;

  constructor(
      private store: Store
  ) {
    this.availability$ = store.select(availabilitySelector);
  }

  public dateFilter(date: Date): boolean {
    const notDisponibleDate = unavailibilities;
    if (date.getTime() < Date.now()) {
      return false;
    }
    if (date.getDay() ===  0 || date.getDay() === 6) {
      return false;
    }
    for (const d of notDisponibleDate) {
      if (d.getDate() === date.getDate() && d.getMonth() === date.getMonth() && d.getFullYear() === date.getFullYear()) {
        return false;
      }
    }
    return true;
  }
}
