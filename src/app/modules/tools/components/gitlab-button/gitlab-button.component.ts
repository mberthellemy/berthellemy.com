import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'app-gitlab-button',
  templateUrl: './gitlab-button.component.html',
  styleUrls: ['./gitlab-button.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GitlabButtonComponent {
  @Input() size: 'small' | 'medium' | 'large' = 'medium';

  public goToMyGitlab(): void {
    window.open('https://gitlab.com/mberthellemy', '_blank');
  }
}
