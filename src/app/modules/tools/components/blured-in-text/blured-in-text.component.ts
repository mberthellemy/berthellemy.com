import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {Observable} from 'rxjs';
import {Lang} from '../../../../models/Lang';
import {LangText} from '../../../../models/LangText';
import {LangService} from '../../../../services/lang.service';

@Component({
  selector: 'app-blured-in-text',
  templateUrl: './blured-in-text.component.html',
  styleUrls: ['./blured-in-text.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('translation', [
      transition('* => *', [
        style({color: 'transparent', 'text-shadow': '0 0 5px rgba(0,0,0,0.5)'}),
        animate('500ms ease-out', style('*'))
      ])
    ])
  ]
})
export class BluredInTextComponent {
  @Input() public text: string;
}
