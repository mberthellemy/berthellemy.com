import {Component, ElementRef, HostListener, Input} from '@angular/core';

@Component({
  selector: 'app-paragraph',
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.css'],
})
export class ParagraphComponent {
  public inBoundaries: boolean;
  @Input() isCard = false;
  @Input() imgLink: string;
  @Input() titleAlign: 'left' | 'center' | 'right' = 'left';
  @Input() title: string;
  @Input() p1: string;
  @Input() p2: string;

  constructor(
      private elRef: ElementRef
  ) {}


  @HostListener('window:scroll', ['$event'])
  public onScroll(): void {
    this.inBoundaries = this.isInViewport();
  }

  get isMobile(): boolean {
    return window.innerWidth < 700;
  }

  private isInViewport(): boolean {
    const bounding = this.elRef.nativeElement.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }
}
