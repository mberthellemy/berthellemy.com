import {HttpClient} from '@angular/common/http';
import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {delay} from 'rxjs/operators';
import {Lang} from '../../../../models/Lang';
import {LangText} from '../../../../models/LangText';
import {ContactService} from '../../services/contact.service';
import {LangService} from '../../../../services/lang.service';

@Component({
  selector: 'app-call-to-action',
  templateUrl: './call-to-action.component.html',
  styleUrls: ['./call-to-action.component.css']
})
export class CallToActionComponent {
  @Input() public actionTitle: LangText;
  @Input() public messageLabel;

  public email = '';
  public message = '';
  public invalidEmail = false;
  public invalidMessage = false;
  public sendStatus = false;
  public lang$: Observable<Lang>;

  constructor(
    private translateService: TranslateService,
    private langService: LangService,
    private matSnackBar: MatSnackBar,
    private contactService: ContactService
  ) {
    this.lang$ = langService.lang$();
  }


  get sendIcon(): string {
    if (this.sendStatus === undefined) {
      return 'hourglass_empty';
    }
    if (this.sendStatus === true) {
      return 'checked';
    }
    if (this.sendStatus === false) {
      return 'send';
    }
  }

  public async send(lang: Lang): Promise<void> {
    if (this.sendStatus !== false) {
      return;
    }
    this.sendStatus = undefined;
    this.invalidEmail = false;
    this.invalidMessage = false;
    if (!this.email || (this.email && !this.email.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/))) {
      this.sendStatus = false;
      this.invalidEmail = true;
      this.invalidMessage = false;
      document.getElementById('email').focus();
      const message = await this.translateService.get('tools.CTA.noEmailProvided').toPromise();
      this.matSnackBar.open(message, null, {duration: 2000});
    } else if (!this.message) {
      this.sendStatus = false;
      this.invalidEmail = false;
      this.invalidMessage = true;
      document.getElementById('message').focus();
      const message = await this.translateService.get('tools.CTA.noMessageProvided').toPromise();
      this.matSnackBar.open(message, null, {duration: 2000});
    } else {
      this.invalidEmail = false;
      this.invalidMessage = false;
      await this.contactService.sendMessage(this.email, this.message).toPromise();
      const message = await this.translateService.get('tools.CTA.messageSent').toPromise();
      this.sendStatus = true;
      this.matSnackBar.open(message, null, {duration: 2000});
    }
  }
}
