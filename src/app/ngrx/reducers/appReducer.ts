import {createReducer, on} from '@ngrx/store';
import {setLangAction} from '../actions/lang.action';
import {
    clearSelectedKeywordsAction,
    defineSelectedKeywordsAction,
    showHeatmapAction,
    showSelectedAction
} from '../actions/keyword.action';
import {AppState, initialState, RootState} from '../states/root.state';


export const appReducer = createReducer(initialState,
    on(setLangAction, (state: AppState, {lang}) => {
        return {...state, lang};
    }),
    on(showHeatmapAction, (state: AppState, {show}) => {
        return {...state, showHeatmapTag: show, showSelectedTags: false};
    }),
    on(showSelectedAction, (state: AppState, {show}) => {
        return {...state, showSelectedTags: show, showHeatmapTag: false};
    }),
    on(defineSelectedKeywordsAction, (state: AppState, {selectedKeywords}) => {
        if (state.showSelectedTags === false) {
            return state;
        }
        return {...state, selectedKeywords};
    }),
    on(clearSelectedKeywordsAction, (state: AppState) => {
        return {...state, selectedKeywords: []};
    })
);
