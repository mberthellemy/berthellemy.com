import {createSelector} from '@ngrx/store';
import {appSelector} from './app.selector';

export const availabilitySelector = createSelector(appSelector, (state) => state.availability);
