import {RootState} from '../states/root.state';

export const appSelector = (state: RootState) => state.app;
