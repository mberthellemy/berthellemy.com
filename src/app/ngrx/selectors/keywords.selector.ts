import {createSelector} from '@ngrx/store';
import {appSelector} from './app.selector';

export const showHeatmapSelector = createSelector(appSelector, (state) => state.showHeatmapTag);
export const showSelectedSelector = createSelector(appSelector, (state) => state.showSelectedTags);
export const selectedKeywordsSelector = createSelector(appSelector, (state) => state.selectedKeywords);
