import {createSelector} from '@ngrx/store';
import {AppState} from '../states/root.state';
import {appSelector} from './app.selector';

export const langSelector = createSelector(appSelector, (state: AppState) => state.lang) ;
