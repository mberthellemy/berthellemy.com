import {registerLocaleData} from '@angular/common';
import localeUs from '@angular/common/locales/en';
import localeFr from '@angular/common/locales/fr';
import {Injectable} from '@angular/core';
import {DateAdapter} from '@angular/material/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {Actions, createEffect, ofType, ROOT_EFFECTS_INIT} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {map} from 'rxjs/operators';
import {Lang} from '../../models/Lang';
import {LangService} from '../../services/lang.service';
import {setLangAction} from '../actions/lang.action';
import {langSelector} from '../selectors/lang.selector';
import {RootState} from '../states/root.state';


@Injectable()
export class LangEffect {
    constructor(
        private titleService: Title,
        private store$: Store<RootState>,
        private actions$: Actions,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
        private langService: LangService,
        private dateAdapter: DateAdapter<Date>,
    ) {}


    public init$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ROOT_EFFECTS_INIT),
            map(() => {
                const lang = this.getLang();
                return setLangAction({lang});
            }))
    );

    public langSetted$ = createEffect(() => {
        return this.store$.select(langSelector).pipe(map((lang: Lang) => {
            this.translateService.use(lang);
            this.setLocale(lang);
            this.setTitle(lang);
        }));
    }, {dispatch: false});

    private getLang(): Lang {
        const url = window.location;
        const firstSubdomain = url.host.split('.')[0];
        if (firstSubdomain === 'fr') {
            return Lang.FRENCH;
        } else if (firstSubdomain === 'en') {
            return Lang.ENGLISH;
        }

        const navigatorLang = navigator.language;
        if (navigatorLang === Lang.FRENCH) {
            return Lang.FRENCH;
        } else if (navigatorLang === Lang.ENGLISH) {
            return Lang.ENGLISH;
        }

        return Lang.ENGLISH;
    }

    private setLocale(lang: Lang): void {
        if (lang === Lang.FRENCH) {
            registerLocaleData(localeFr, 'fr-FR');
        } else if (lang === Lang.ENGLISH) {
            registerLocaleData(localeUs, 'en-US');
        }
        this.dateAdapter.setLocale(lang);
    }

    private setTitle(lang: Lang): void {
        if (lang === Lang.FRENCH) {
            this.titleService.setTitle('MB. Dévelopeur Freelance');
        } else if (lang === Lang.ENGLISH) {
            this.titleService.setTitle('MB. Freelance developper');
        }
    }
}


