import {RouterReducerState} from '@ngrx/router-store';
import {Availability} from '../../models/Availability';
import {Lang} from '../../models/Lang';


export interface AppState {
  availability: Availability;
  showHeatmapTag: boolean;
  showSelectedTags: boolean;
  selectedKeywords: string[];
  lang: Lang;
}


export interface RootState {
  router: RouterReducerState;
  app: AppState;
}


export const initialState: AppState = {
  availability: Availability.NOT_AVAILABLE,
  showHeatmapTag: false,
  showSelectedTags: false,
  selectedKeywords: [],
  lang: undefined
};
