import {createAction, props} from '@ngrx/store';
import {Lang} from '../../models/Lang';

export const setLangAction = createAction('[Lang] set', props<{ lang: Lang }>());
