import {createAction, props} from '@ngrx/store';
import {Lang} from '../../models/Lang';

export const showHeatmapAction = createAction('[Keyword] heatmap show', props<{ show: boolean }>());
export const showSelectedAction = createAction('[Keyword] selected show', props<{ show: boolean }>());
export const defineSelectedKeywordsAction = createAction('[Keyword] define selection', props<{ selectedKeywords: string[] }>());
export const clearSelectedKeywordsAction = createAction('[Keyword] clear selection');
