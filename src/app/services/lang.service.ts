import {DOCUMENT} from '@angular/common';
import {Inject, Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Lang} from '../models/Lang';
import {langSelector} from '../ngrx/selectors/lang.selector';

@Injectable()
export class LangService {
  constructor(
    private store: Store,
    @Inject(DOCUMENT) private document: any,
    private translateService: TranslateService
  ) {
    translateService.setDefaultLang(navigator.language);
  }

  public lang$(): Observable<Lang> {
    return this.store.select(langSelector).pipe(map((lang: string) => lang as Lang));
  }

  public changeLang(lang: Lang): void {
    let url = this.document.location.href as string;
    url = url.replace('://www.', '://');
    url = url.replace('://en.', '://');
    url = url.replace('://fr.', '://');
    const tokens = url.split('://');
    if (lang === Lang.FRENCH) {
      const newUrl = tokens[0] + '://fr.' + tokens[1];
      window.location.href = newUrl;
    } else if (lang === Lang.ENGLISH) {
      const newUrl = tokens[0] + '://en.' + tokens[1];
      window.location.href = newUrl;
    }
  }
}
