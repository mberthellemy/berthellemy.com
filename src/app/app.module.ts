import {CommonModule} from '@angular/common';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatTooltipModule} from '@angular/material/tooltip';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {EffectsModule} from '@ngrx/effects';
import {routerReducer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {environment} from '../environments/environment';
import {FlagsComponent} from './components/flags/flags.component';
import {FooterComponent} from './components/footer/footer.component';
import {HeaderComponent} from './components/header/header.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {RootComponent} from './components/root.component';
import {HttpLoaderFactory} from './HttpLoaderFactory';
import {LangEffect} from './ngrx/effects/lang.effect';
import {appReducer} from './ngrx/reducers/appReducer';
import {ShufflePipe} from './pipe/shuffle.pipe';
import {LangService} from './services/lang.service';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/home'},
  {path: 'home', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule) },
  {path: 'skills', loadChildren: () => import('./modules/skills/skills.module').then(m => m.SkillsModule)},
  {path: 'experiences', loadChildren: () => import('./modules/experiences/experiences.module').then(m => m.ExperiencesModule) },
  {path: 'contact', loadChildren: () => import('./modules/contact/contact.module').then(m => m.ContactModule) },
  {path: 'others', loadChildren: () => import('./modules/others/others.module').then(m => m.OthersModule) },
  {path: 'not-found', component: NotFoundComponent},
  {path: '**', pathMatch: 'full', redirectTo: '/not-found'}
];

@NgModule({
  declarations: [
    FlagsComponent,
    RootComponent,
    NavbarComponent,
    HeaderComponent,
    FooterComponent,
    ShufflePipe,
    NotFoundComponent,
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    MatTooltipModule,
    MatButtonModule,
    MatNativeDateModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    StoreModule.forRoot({}),
    StoreModule.forFeature('router', routerReducer),
    StoreModule.forFeature('app', appReducer),
    EffectsModule.forRoot([LangEffect]),
    StoreRouterConnectingModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
  ],
  bootstrap: [RootComponent],
  providers: [
      {provide: MAT_DATE_LOCALE, useValue: window.location},
      LangService
  ]
 })
export class AppModule {}
