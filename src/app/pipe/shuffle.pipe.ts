import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appShuffle'
})
export class ShufflePipe implements PipeTransform {
  private voyel = ['a', 'e', 'o', 'u', 'y'];

  transform(value: string, shouldShuffle: boolean = true): string {
    if (shouldShuffle !== true) {
      return value;
    }
    let output = '';
    for (let i = 0; i < value.length; i++) {
      let char = value.charAt(i).toLowerCase();
      const swap = Math.random() > 0.8;
      if (this.voyel.includes(char) && swap) {
        const randomIndex = Math.floor(Math.random() * this.voyel.length);
        char = this.voyel[randomIndex];
      }
      const randomCase = Math.random() > 0.8;
      if (randomCase) {
        output += char.toUpperCase();
      } else {
        output += char.toLowerCase();
      }
    }
    return output;
  }
}
