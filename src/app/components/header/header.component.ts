import {animate, sequence, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import { Lang } from 'app/models/Lang';
import {Observable, Subscription} from 'rxjs';
import {Availability} from '../../models/Availability';
import {langSelector} from '../../ngrx/selectors/lang.selector';
import {LangService} from '../../services/lang.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [
    trigger('image', [
      transition('* => *', [
        style({filter: 'blur(3px)', opacity: 0.8}),
        animate('100ms', style('*'))
      ])
    ]),
    trigger('wave', [
      transition('* => *', [
        style({transform: 'scale(1.1)', opacity: 1}),
        sequence([
          animate('300ms', style({transform: 'scale(1.5)', opacity: 0})),
          animate('200ms', style({transform: 'scale(1.1)', opacity: 1})),
        ])
      ])
    ])
  ]
})
export class HeaderComponent implements OnDestroy {
  public Availability = Availability;

  @Input() availability: Availability;

  public name = 'Mathieu Berthellemy';

  public imageMePath = './assets/images/me.png';
  public imageSickPath = './assets/images/sick.png';
  public imageThumbUpPath = './assets/images/thumb_up.png';
  public currentImagePath: string = this.imageMePath;
  public coinAngle = 0;
  private subscription: Subscription;

  get isSick(): boolean {
    return this.currentImagePath === this.imageSickPath;
  }

  public Lang = Lang;

  public lang$: Observable<Lang>;

  constructor(
      private langService: LangService
  ) {
    this.lang$ = langService.lang$();
  }

  public ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  public makePermanentThumbUp(): void {
    this.currentImagePath = this.imageThumbUpPath;
  }

  public async makeFlip(): Promise<void> {
    let i = 1;
    let inertie;
    const coeffSpeed = 3;

    do {
      inertie = coeffSpeed / i ;
      const offset = inertie * 360;
      this.coinAngle = (this.coinAngle + offset) % 360;
      await new Promise(resolve => setTimeout(resolve, 25));
      i++;
    } while (inertie > 0.05);

    this.currentImagePath = this.imageSickPath;

    do {
      inertie = coeffSpeed / i;
      const offset = inertie * 360;
      this.coinAngle = (this.coinAngle + offset) % 360;
      await new Promise(resolve => setTimeout(resolve, 25));
      i++;
    } while (this.coinAngle % 360 > 2);
  }
}
