import {Component, OnDestroy} from '@angular/core';
import {Lang} from 'app/models/Lang';
import {Observable} from 'rxjs';
import {LangService} from '../../services/lang.service';

@Component({
  selector: 'app-flags',
  templateUrl: './flags.component.html',
  styleUrls: ['./flags.component.css']
})
export class FlagsComponent {
  public Lang = Lang;
  public lang$: Observable<Lang>;

  constructor(
      private langService: LangService
  ) {
    this.lang$ = langService.lang$();
  }

  public changeLang(lang: Lang): void {
    this.langService.changeLang(lang);
  }
}
