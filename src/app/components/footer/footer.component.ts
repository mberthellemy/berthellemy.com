import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {Lang} from '../../models/Lang';
import {langSelector} from '../../ngrx/selectors/lang.selector';
import {LangService} from '../../services/lang.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent {
  public Lang = Lang;

  public lang$: Observable<Lang>;

  constructor(
      private store: Store,
      private langService: LangService
  ) {
    this.lang$ = store.select(langSelector);
  }

}
