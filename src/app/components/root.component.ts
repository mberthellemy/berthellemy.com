import {animate, style, transition, trigger} from '@angular/animations';
import {Component} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {getSelectors, RouterReducerState} from '@ngrx/router-store';
import {createFeatureSelector, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Availability} from '../models/Availability';
import {Lang} from '../models/Lang';
import {availabilitySelector} from '../ngrx/selectors/informations.selector';

export const selectRouter = createFeatureSelector<RouterReducerState>('router');

export const {
  selectCurrentRoute,
} = getSelectors(selectRouter);

const leftShift = [
  style({ transform: 'translateX(50px)', opacity: 0 }),
  animate('200ms', style('*'))
];


const rightShift = [
  style({ transform: 'translateX(-50px)', opacity: 0 }),
  animate('200ms', style('*'))
];


const bottomShift = [
  style({ transform: 'translateY(50px)', opacity: 0 }),
  animate('200ms', style('*'))
];


const topShift = [
  style({ transform: 'translateY(-50px)', opacity: 0 }),
  animate('200ms', style('*'))
];

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css'],
  animations: [
    trigger('routeAnimations', [
      transition('void => *', bottomShift),
      transition('home => *', leftShift),
      transition('* => home', rightShift),
      transition('* => contact', leftShift),
      transition('contact => *', rightShift),
      transition('reviews => projects', topShift),
      transition('projects => reviews', bottomShift),
      transition('frontend => backend', bottomShift),
      transition('backend => frontend', topShift),
      transition('frontend => teamwork', topShift),
      transition('teamwork => frontend', bottomShift),
      transition('backend => teamwork', topShift),
      transition('teamwork => backend', bottomShift),
      transition('frontend => projects', leftShift),
      transition('frontend => reviews', leftShift),
      transition('projects => frontend', rightShift),
      transition('reviews => frontend', rightShift),
      transition('backend => projects', leftShift),
      transition('backend => reviews', leftShift),
      transition('projects => backend', rightShift),
      transition('reviews => backend', rightShift),
      transition('teamwork => projects', leftShift),
      transition('teamwork => projects', leftShift),
      transition('teamwork => reviews', leftShift),
      transition('projects => teamwork', rightShift),
      transition('reviews => teamwork', rightShift),
    ])
  ]
})
export class RootComponent {
  public Lang = Lang;

  public route$: Observable<string>;
  public availability$: Observable<Availability>;

  constructor(
      private store: Store,
      private titleService: Title,
      private router: Router,
  ) {
    this.availability$ = store.select(availabilitySelector);
    this.route$ = this.router.events
      .pipe(filter((event: any) => event.url !== undefined))
      .pipe(map((event: {url: string}) => this.extractStateNameFromUrl(event.url)));
  }

  private extractStateNameFromUrl(url: string): string {
    const token = url.split('/');
    return token.pop();
  }
}
