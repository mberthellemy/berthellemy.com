import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent {
  get isMobile(): boolean {
    return window.innerWidth < 760;
  }
}
