
export enum Availability {
  AVAILABLE = 'Available',
  NOT_AVAILABLE = 'Not available'
}
