

export interface Review {
  i18nKey: string;
  name: string;
  company: string;
  year: number;
  linkedinLink?: string;
}
