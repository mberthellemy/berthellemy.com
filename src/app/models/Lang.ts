
export enum Lang {
  FRENCH = 'fr-FR',
  ENGLISH = 'en-US'
}
