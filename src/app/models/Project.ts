export interface Project {
  i18nKey: string;
  type: 'project' | 'event';
  date: {
    start: Date,
    end?: Date
  };
  imagePath?: string;
  tags: string[];
  icon: string;
  source?: string;
  link?: string;
}
