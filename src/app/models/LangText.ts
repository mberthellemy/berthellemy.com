import {Lang} from './Lang';

export interface LangText {
  [Lang.ENGLISH]: string;
  [Lang.FRENCH]: string;
}
